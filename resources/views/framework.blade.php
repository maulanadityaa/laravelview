@extends('layout/layout')

@section('title', 'About')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">
                <h1>Jenis Framework PHP</h1>

                @if (count($nama))
                    <ul>
                        @foreach ($nama as $a)
                            <li>{{ $a }}</li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
    </div>
@endsection
