@extends('layout/layout')

@section('title', 'Home')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">
                <h1 class="mt-4">Hello, world!</h1>
            </div>
        </div>
    </div>
@endsection
